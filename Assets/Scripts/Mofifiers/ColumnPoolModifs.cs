﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;



public class ColumnPoolModifs : MonoBehaviour
{
    #region Inner classes
    [System.Serializable]
    public class DifficultyConfig
    {
        public int Score;
        public SpawnConfig Config;
    }

    public GameObject columnPrefab;
    public int columnPoolSize = 5;


    private GameObject[] columns;
    private int currentColumn = 0;
    public List<DifficultyConfig> DifficultyLevels;
    
    [System.Serializable]
    public class SpawnConfig
    {
        ///  Scroll speed of the game
        [Range(0.5f, 2.5f)]
        public float ScrollSpeed = 1;
        ///  Spawn position X of the next column
        public float StartX = 5;
        ///  Min spawn pos X of the next column
        public float SpawnMin = 4;
        /// Max spawn pos X of the next column
        public float SpawnMax = 7;
        /// Min spawn pos Y of the next column
        public float HMin = -1;
        ///  Max spawn pos Y of the next column
        public float HMax = 3.5f;
        /// Min spacing between the top and bottom column
        public float SpacingMin = 2f;
        /// Max spacing between the top and bottom column
        public float SpacingMax = 3.5f;
        ///  Chance of a column to have a top and bottom part
        [Range(0, 1)]
        public float PrctDouble = 0.5f;
    }
    #endregion

    void Start()
    {
        columns = new GameObject[columnPoolSize];

        for (int i = 0; i < columnPoolSize; i++)
        {

            columns[i] = Instantiate(columnPrefab);

            columns[i].SetActive(false);
        }
       

        SpawnColumn(DifficultyLevels[0].Config, 0);
    }

    void SpawnColumn(SpawnConfig config, float lastColumnPositionX)
    {

        var nextColumn = currentColumn + 1 >= columnPoolSize ? 0 : currentColumn + 1;
        var column = columns[nextColumn];
        column.SetActive(true);

        Vector3 position = new Vector3(lastColumnPositionX, 0, 0);
        position.x += Random.Range(config.SpawnMin, config.SpawnMax);
        position.y += Random.Range(config.HMin, config.HMax);
        column.transform.position = position;

        bool IsDouble = Random.Range(0f, 1f) < config.PrctDouble;

        foreach (Transform child in column.transform)
        {
            child.gameObject.SetActive(true);
        }

        if (!IsDouble)
        {
            var childCount = column.transform.childCount;
            var hidePart = column.transform.GetChild(Random.Range(0, childCount));
            hidePart.gameObject.SetActive(false);
        }

        var spacing = Random.Range(config.SpacingMin, config.SpacingMax);
        column.transform.GetChild(0).localPosition = Vector3.up * spacing * 0.5f;
        column.transform.GetChild(1).localPosition = Vector3.down * spacing * 0.5f;

        currentColumn = nextColumn;
    }
    private void Update()
    {
        if (columns[currentColumn].transform.position.x <= 8)
            {
            // Algo pour trouver la difficulté
            var difficulty = DifficultyLevels
                // On ordonne de manière décroissante en fonction du score
                .OrderByDescending(c => c.Score)
                // On choisit le premier niveau de difficulté avec un score plus petit que le notre
                .FirstOrDefault(c => c.Score <= GameControl.instance.Score);
            // On utilse la configuration du niveau de difficulté pour créer le
            // prochain pilier
            SpawnColumn(difficulty.Config, columns[currentColumn].transform.position.x);
            }

    }
}